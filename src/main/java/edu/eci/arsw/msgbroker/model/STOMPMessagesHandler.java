/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.eci.arsw.msgbroker.model;

import java.util.LinkedList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

/**
 *
 * @author 2091412
 */
@Controller
public class STOMPMessagesHandler {

    @Autowired
    ListaPuntos listPt;

    @MessageMapping("/newpoint")    
    public void getLine(Point pt) throws Exception {
        System.out.println("Nuevo punto recibido en el servidor!:"+pt);
        System.out.println("evento ;D");
        listPt.add(pt);
    }
}


