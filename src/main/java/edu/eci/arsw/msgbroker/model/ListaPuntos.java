/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.eci.arsw.msgbroker.model;

import java.util.LinkedList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

/**
 *
 * @author 2091412
 */
@Service
public class ListaPuntos {
    
    @Autowired
    SimpMessagingTemplate msgt;
    
    LinkedList<Point> listPt = new LinkedList();
    
    public void add(Point pt){
        msgt.convertAndSend("/topic/newpoint", pt);
        
        if(listPt.size() < 4){
            listPt.add(pt);
            if(listPt.size() == 4){
                msgt.convertAndSend("/topic/newpolygon", listPt);
                listPt.clear();
            }
        }
    }
    
}
