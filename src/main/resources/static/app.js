var stompClient = null;
var context;
var canvas;

function connect() {
    var socket = new SockJS('/stompendpoint');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        console.log('Connected: ' + frame);
        
        stompClient.subscribe('/topic/newpoint', function (data) {
            var theObject=JSON.parse(data.body); 
            context.beginPath();
            context.arc(theObject.x,theObject.y,1,0,2*Math.PI);
            context.stroke();
        });
            
        stompClient.subscribe('/topic/newpolygon', function (data) {
            var theObject = [];
            theObject = JSON.parse(data.body); 
            var c2 = canvas.getContext('2d');
            context.fillStyle = '#f00';
            context.beginPath();
            context.moveTo(theObject[0].x, theObject[0].y);
            context.lineTo(theObject[1].x, theObject[1].y);
            context.lineTo(theObject[2].x, theObject[2].y);
            context.lineTo(theObject[3].x, theObject[3].y);
            context.closePath();
            context.fill();

        });
    });
}
function sendPoint(){
    stompClient.send("/app/newpoint", {}, JSON.stringify({x:$("#x").val(),y:$("#y").val()}));
    
}

function disconnect() {
    if (stompClient != null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}


$(document).ready(
        function () {
            
            console.info('connecting to websockets');
            $( "#send" ).click(function() { sendPoint(); });
            $( "#connect" ).click(function() { connect(); });
            $( "#disconnect" ).click(function() { disconnect(); });
            
            canvas = document.getElementById('myCanvas');
            context = canvas.getContext('2d');
            function getMousePos(canvas, evt) {
                 var rect = canvas.getBoundingClientRect();
            return {
                x: evt.clientX - rect.left,
                y: evt.clientY - rect.top
             };
            }
            
            
            canvas.addEventListener('mousedown', function(evt) {
                var mousePos = getMousePos(canvas, evt);
                document.getElementById("x").value=mousePos.x;
                document.getElementById("y").value=mousePos.y;
                sendPoint();
            }, false);
            
            canvas.addEventListener("touchstart", function(evt) {
                
                evt.preventDefault();
                
                canvas_x = event.targetTouches[0].pageX;
                canvas_y = event.targetTouches[0].pageY;
                
                document.getElementById("x").value=canvas_x;
                document.getElementById("y").value=canvas_y;
                
                sendPoint();
            }, false);
            
        }
  

           
     
                
);
